'use strict';
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  FlatList,
  Image,
  Dimensions,
  Alert,
  Platform,
  NativeModules,
  TouchableOpacity,
} from 'react-native';

import Carousel, {Pagination} from 'react-native-snap-carousel';
import Ionicons from 'react-native-vector-icons/Ionicons';
import YogaIcon from './src/assets/svg/yoga.svg';
import CloseIcon from './src/assets/svg/close-icon.svg';

const marginTopStatusBar =
  Platform.OS === 'ios' ? NativeModules.StatusBarManager.HEIGHT : 20;

const {width: viewportWidth, height: viewportHeight} = Dimensions.get('window');

const wp = (percentage) => {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
};

const sliderWidth = viewportWidth;
const slideWidth = wp(94);
const itemHorizontalMargin = wp(1);
const itemWidth = slideWidth + itemHorizontalMargin * 2;

const AppColors = {
  inactive: '#A9A9A9',
  inActive2: '#EDEDED',
  active: '#090909',
  positive: '#FF7700',
  light: 'white',
  stable: '#00673C',
  assertive: '#DC2525',
};

const packagesData = [
  {
    popular: true,
    promoLength: 12,
    promoPrice: 150,
    regularPrice: 15.0,
    percentageDiscount: -45,
  },
  {
    promoLength: 6,
    promoPrice: 99,
    regularPrice: 15.5,
    percentageDiscount: -58,
  },
  {
    promoLength: 1,
    promoPrice: 30,
    regularPrice: 1,
    isDays: true,
    isNoDiscount: true,
  },
];

const carouselSlidesData = [
  {
    src:
      'https://cms-assets.tutsplus.com/uploads/users/2660/posts/32516/image/graffiti-stencil-logo-generator-for-a-crossfit-gym-2997c.jpg',
    title: 'Subscribe',
    description: 'This is a subtitle text no more then 2 rows on this one.',
  },
  {
    src:
      'https://cms-assets.tutsplus.com/uploads/users/2660/posts/32516/image/online-logo-generator-for-a-power-fitness-gym-2457j-93-el.jpg',
    title: 'Subscribe',
    description: 'This is a subtitle text no more then 2 rows on this one.',
  },
  {
    src:
      'https://cms-assets.tutsplus.com/uploads/users/151/posts/32516/image/Placeit8.jpg',
    title: 'Subscribe',
    description: 'This is a subtitle text no more then 2 rows on this one.',
  },
];

const AppText = (props) => {
  const textStyle = [
    // {
    //   fontFamily:
    //     Platform.OS === 'ios'
    //       ? 'Montserrat-Regular'
    //       : 'montserrat-regular-webfont',
    // },
    props.style,
  ];
  return (
    <Text {...props} style={textStyle}>
      {props.children}
    </Text>
  );
};

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      packages: packagesData,
      carouselSlides: carouselSlidesData,
      activeSubcriptionIndex: 0,
      isShowCloseBtn: true,
      activeSlide: 0,
      isSubcriptionActive: true,
    };
  }

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        isShowCloseBtn: true,
      });
    }, 5000);
  }

  componentDidUpdate(prevProps, prevState) {}

  /** renders the terms and conditions **/
  _renderFooter() {
    return (
      <View style={styles.footerCon}>
        <AppText style={styles.footerTitle}>
          This is terms and condition text.
        </AppText>
        <AppText style={styles.footerDesc}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco l aboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </AppText>
      </View>
    );
  }

  /** contains the close button **/
  _renderHeader() {
    const {isShowCloseBtn} = this.state;
    return (
      <View style={styles.headerCon}>
        {isShowCloseBtn && (
          <TouchableOpacity
            style={styles.clsBtn}
            onPress={() => Alert.alert('Close!')}>
            <CloseIcon />
          </TouchableOpacity>
        )}
      </View>
    );
  }

  _renderPagination() {
    const {carouselSlides} = this.state;
    const {activeSlide} = this.state;

    if (carouselSlides.length < 2) {
      return null;
    }

    return (
      <Pagination
        dotsLength={carouselSlides.length}
        activeDotIndex={activeSlide}
        dotContainerStyle={styles.dotContainerStyle}
        containerStyle={styles.paginationContainerStyle}
        dotStyle={[styles.dotStyle]}
        inactiveDotStyle={styles.inactiveDotScale}
        inactiveDotOpacity={0.4}
        inactiveDotScale={1}
      />
    );
  }

  _renderCarouselSection() {
    const {carouselSlides} = this.state;
    const isShowPagination = carouselSlides.length > 1;
    return (
      <View style={styles.carouselCon}>
        <Carousel
          loop
          data={carouselSlides}
          renderItem={this._renderCarouselItem.bind(this)}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          onSnapToItem={(index) => this.setState({activeSlide: index})}
          autoplay
          autoplayInterval={3000}
        />
        {isShowPagination && this._renderPagination()}
      </View>
    );
  }

  _renderCarouselItem({item, index}) {
    return (
      <View style={styles.carouselItemCon}>
        <View style={styles.carouselImg}>
          <YogaIcon height={50} width={50} style={{left: 5}} />
        </View>
        <AppText style={[styles.carouselTitle, styles.boldText]}>
          {item.title}
        </AppText>
        <AppText style={styles.carouselDesc}>{item.description}</AppText>
      </View>
    );
  }

  _renderList() {
    const {packages} = this.state;
    return (
      <View style={styles.listParentCon}>
        <FlatList
          data={packages}
          renderItem={this._renderListItem.bind(this)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }

  _renderListItem({item, index}) {
    const {activeSubcriptionIndex} = this.state;
    const isIndexActive = index === activeSubcriptionIndex;
    const promoTypePeriod = item.isDays ? 'Month' : 'MONTHS';
    const promoTypePerPeriod = item.isDays ? 'DAY' : 'MONTH';

    const promoLengthText = `${item.promoLength} ${promoTypePeriod}`;
    const promoPriceText = `${Number(item.promoPrice).toFixed(2)} $`;
    const regularPrice = `${Number(item.regularPrice).toFixed(
      2,
    )} $ / ${promoTypePerPeriod}`;
    const percentageText = item.percentageDiscount
      ? `${item.percentageDiscount}%`
      : null;

    const discountPriceText = `${Number(
      item.regularPrice * item.promoLength,
    ).toFixed(2)} $/year`;
    const parentConStyle = [
      styles.listCon,
      isIndexActive
        ? {backgroundColor: AppColors.active}
        : {backgroundColor: AppColors.inActive2},
    ];

    const textStyling = isIndexActive
      ? {color: AppColors.light}
      : {color: AppColors.inactive};
    const touchConStyle = [{marginTop: index === 0 ? 10 : 2}];

    return (
      <TouchableOpacity
        key={index}
        onPress={() => this.setState({activeSubcriptionIndex: index})}
        style={touchConStyle}>
        <View style={{opacity: isIndexActive ? 1 : 0.7}}>
          {item.popular && (
            <View style={styles.subscriptionPopularCon}>
              <AppText style={styles.subscriptionPopularText}>POPULAR</AppText>
            </View>
          )}
          <View style={parentConStyle}>
            {percentageText &&
              <View style={styles.cornerCon}>
                <Text style={styles.cornerText}>{percentageText}</Text>
              </View>
            }
            <View style={styles.listColumn}>
              <AppText style={[styles.listHeaderText, textStyling]}>
                {promoLengthText}
              </AppText>
              <AppText style={[styles.listBottomText, textStyling]}>
                {promoPriceText}
              </AppText>
            </View>
            <View style={[styles.listColumn, styles.rightColumn]}>
              <AppText
                style={[
                  styles.listHeaderText,
                  styles.rightColumnText,
                  textStyling,
                ]}>
                {regularPrice}
              </AppText>
              {!item.isNoDiscount && (
                <AppText
                  style={[
                    styles.rightColumnText,
                    styles.listBottomText,
                    styles.listBottomRightText,
                    textStyling,
                  ]}>
                  {discountPriceText}
                </AppText>
              )}
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  }

  _renderContinue() {
    return (
      <View style={styles.continueCon}>
        <AppText style={[styles.smallText]}>
          Test for free for 7 days and you can cancel anytime
        </AppText>
        <TouchableOpacity
          style={styles.submitBtn}
          onPress={() => Alert.alert('Finish the test')}>
          <AppText style={styles.submitBtnText}>Continue</AppText>
        </TouchableOpacity>
      </View>
    );
  }

  _renderSubscriptionInfo() {
    const {isSubcriptionActive} = this.state;
    const textBoldStyle = {fontWeight: 'bold'};
    const subscriptionText = isSubcriptionActive ? (
      <AppText style={styles.subscriptionTextStyle}>
        <AppText style={textBoldStyle}>7 days free</AppText> for all the
        subscription packages
      </AppText>
    ) : (
      <AppText style={styles.subscriptionTextStyle}>
        Your subscription expired{' '}
        <AppText style={textBoldStyle}>24 days ago'</AppText>
      </AppText>
    );
    return subscriptionText;
  }

  render() {
    return (
      <>
        <StatusBar barStyle="dark-content" />
        <SafeAreaView>
          {this._renderHeader()}
          <ScrollView
            contentInsetAdjustmentBehavior="automatic"
            style={styles.scrollView}>
            {this._renderCarouselSection()}
            {this._renderSubscriptionInfo()}
            {this._renderList()}
            {this._renderContinue()}
            {this._renderFooter()}
          </ScrollView>
        </SafeAreaView>
      </>
    );
  }
}

const styles = StyleSheet.create({
  scrollView: {
    height: '100%',
    width: '100%',
  },
  headerCon: {
    position: 'absolute',
    // backgroundColor: 'red',
    width: '100%',
    zIndex: 2,
    marginTop: marginTopStatusBar,
  },
  clsBtn: {
    alignSelf: 'flex-end',
    backgroundColor: '#EDEDED',
    borderRadius: 100,
    marginRight: 10,
    height: 40,
    width: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  carouselCon: {
    paddingTop: 30,
  },
  carouselItemCon: {
    paddingHorizontal: 20,
    alignItems: 'center',
    width: '80%',
    alignSelf: 'center',
  },
  carouselImg: {
    height: 90,
    width: 90,
    resizeMode: 'contain',
    borderRadius: 200,
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: AppColors.positive,
    backgroundColor: AppColors.positive,
  },
  carouselDesc: {
    textAlign: 'center',
  },
  boldText: {
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 20,
    marginBottom: 10,
  },
  smallText: {
    fontWeight: '400',
    fontSize: 10,
  },
  submitBtn: {
    color: AppColors.light,
    backgroundColor: AppColors.positive,
    width: '90%',
    alignItems: 'center',
    paddingVertical: 15,
    marginVertical: 10,
    borderTopLeftRadius: 10,
    borderBottomRightRadius: 10,
  },
  submitBtnText: {
    color: AppColors.light,
    fontWeight: '700',
    fontSize: 18,
  },
  subscriptionTextStyle: {
    textAlign: 'center',
    backgroundColor: '#D5F1C9',
    paddingVertical: 13,
    // marginVertical: 5,
    color: AppColors.stable,
  },
  continueCon: {
    alignItems: 'center',
    // paddingVertical: 0,
  },
  footerCon: {
    alignItems: 'center',
    textAlign: 'center',
    paddingHorizontal: 20,
    marginBottom: 20,
  },
  footerTitle: {
    marginVertical: 5,
    fontSize: 10,
    fontWeight: '600',
  },
  footerDesc: {
    fontSize: 11,
    textAlign: 'center',
    fontWeight: '300',
  },
  listParentCon: {
    marginVertical: 15,
    paddingHorizontal: 15,
    // paddingTop: 30,
  },
  listCon: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 20,
    paddingHorizontal: 20,
    marginVertical: 3,
    backgroundColor: AppColors.inActive2,
    borderRadius: 5,
    overflow: 'hidden',
  },
  listColumn: {
    marginBottom: 5,
    justifyContent: 'center',
  },
  listHeaderText: {
    fontSize: 16,
    fontWeight: '700',
    marginBottom: 5,
  },
  listBottomText: {
    fontSize: 11,
    fontWeight: '300',
  },
  listBottomRightText: {
    textDecorationLine: 'line-through',
    textDecorationStyle: 'solid',
  },
  rightColumn: {
    textAlign: 'right',
  },
  rightColumnText: {
    textAlign: 'right',
  },
  subscriptionPopularCon: {
    backgroundColor: AppColors.positive,
    paddingHorizontal: 15,
    paddingVertical: 5,
    borderRadius: 10,
    position: 'absolute',
    alignSelf: 'center',
    zIndex: 2,
    top: -10,
  },
  subscriptionPopularText: {
    color: AppColors.light,
    fontWeight: '700',
    fontSize: 12,
  },
  cornerCon: {
    position: 'absolute',
    backgroundColor: AppColors.positive,
    height: 50,
    width: 90,
    right: -41,
    justifyContent: 'flex-end',
    alignItems: 'center',
    transform: [{rotateZ: '40deg'}],
    top: -23,
  },
  cornerText: {
    color: AppColors.light,
    fontWeight: '600',
    fontSize: 12,
    // alignSelf: 'center'
  },
});

export default App;
